package com.example.hakan.jsonusercontroll;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText mail,sifre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mail = (EditText) findViewById(R.id.txtMail);
        sifre = (EditText) findViewById(R.id.txtSifre);
    }


    public void fncCalis(View v) {

        String gmail = mail.getText().toString().trim();
        String gsifre = sifre.getText().toString().trim();
        String urll = "http://dinles.com/rentecar/json/kullanici_oku.php?mail="+gmail+"&sifre="+gsifre+"";

        new data(urll).execute();
    }



    class data extends AsyncTask<String, String, String> {

        String gurl = "";
        public data(String urll) {
            gurl = urll;
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder buil = new StringBuilder();
            try {

                HttpURLConnection conn;
                URL url = new URL(gurl);
                conn = (HttpURLConnection) url.openConnection();
                InputStream st =new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(st));
                String line;
                while ((line = reader.readLine()) != null) {
                    buil.append(line);
                }

            }catch (Exception ex){
                Log.d("Bağlantı Hatası : ", ex.toString());
            }

            return buil.toString();
        }



        String durum = "";
        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            try {
                JSONObject obj = new JSONObject(aVoid);

                if(!obj.isNull("id")) {
                    // Giriş Başarılı
                    durum = obj.getString("adi").toString() + " " + obj.getString("soyadi").toString();
                }else {
                    durum = obj.getString("durum");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("Gelen Data ", durum);
            Toast.makeText(getApplicationContext(), durum, Toast.LENGTH_SHORT).show();
        }
    }


}
